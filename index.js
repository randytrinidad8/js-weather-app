window.addEventListener("load", () => {
    let long;
    let lat;
    let temperatureDescription = document.querySelector('.temperature-description');
    let temperatureDegree = document.querySelector('.temperature-degree');
    let locationTimezone = document.querySelector('.location-timezone');
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
            long = position.coords.longitude;
            lat = position.coords.latitude;
            const proxy = "https://cors-anywhere.herokuapp.com/";
            const api = `${proxy}https://api.darksky.net/forecast/ee6f74b50670eadb56d4d2e3b609ef59/${lat},${long}`;
        
            fetch(api)
            .then(response => {
                return response.json();
            })
            .then(data => {
                
                const {temperature, summary} = data.currently;
                //Set DOM Elements from the api
                temperatureDegree.textContent = temperature;
                locationTimezone.textContent = data.timezone;
                temperatureDescription.textContent = summary;
            })
        });

      
    }
});